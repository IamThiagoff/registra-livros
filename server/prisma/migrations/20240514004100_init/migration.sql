-- AlterTable
ALTER TABLE "Book" ADD COLUMN     "cover" TEXT,
ADD COLUMN     "currentChapter" TEXT,
ADD COLUMN     "currentPage" INTEGER;
