-- CreateTable
CREATE TABLE "Book" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "author" TEXT NOT NULL,
    "category" TEXT NOT NULL,
    "chapter" TEXT,
    "page" INTEGER,

    CONSTRAINT "Book_pkey" PRIMARY KEY ("id")
);
