// src/schemas/bookSchema.ts
import { z } from 'zod';

const bookSchema = z.object({
  title: z.string().nonempty(),
  author: z.string().nonempty(),
  category: z.string().nonempty(),
  chapter: z.string().optional(),
  page: z.number().int().positive().optional()
});

export { bookSchema };
