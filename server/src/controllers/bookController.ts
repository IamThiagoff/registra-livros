// src/controllers/bookController.ts

import prisma from '../db/index';

interface BookCreateData {
    title: string;
    author: string;
    category: string;
    cover?: string;
    currentChapter?: string;
    currentPage?: number;
  }

  async function createBook(data: BookCreateData) {
    return await prisma.book.create({
      data
    });
  }
  

async function getAllBooks() {
  return await prisma.book.findMany();
}

async function getBookById(id: number) {
  return await prisma.book.findUnique({
    where: {
      id
    }
  });
}

async function updateBook(id: number, data: { title?: string, author?: string, category?: string, cover?: string, currentChapter?: string, currentPage?: number }) {
  return await prisma.book.update({
    where: {
      id
    },
    data
  });
}

async function deleteBook(id: number) {
  return await prisma.book.delete({
    where: {
      id
    }
  });
}

export { createBook, getAllBooks, getBookById, updateBook, deleteBook };
