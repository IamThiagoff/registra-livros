// src/index.ts

import fastify from 'fastify';
import bookRoutes from './routes/books';

const app = fastify({ logger: true });

// Registrar as rotas
app.register(bookRoutes);

const start = async () => {
  try {
    await app.listen(3000);
  } catch (error) {
    app.log.error(error);
    process.exit(1);
  }
};

start();
