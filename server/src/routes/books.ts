// src/routes/bookRoutes.ts

import { FastifyInstance } from 'fastify';
import { getAllBooks, createBook } from '../controllers/bookController';
import { bookSchema } from '../schemas/bookSchema';

async function bookRoutes(fastify: FastifyInstance) {
  fastify.get('/', async (request, reply) => {
    return { message: 'Bem-vindo à sua aplicação de registro de livros!' };
  });

  fastify.get('/books', async (request, reply) => {
    const books = await getAllBooks();
    return books;
  });

  fastify.post('/books', async (request, reply) => {
    try {
      const bookData = bookSchema.parse(request.body); // Verifica se os dados estão no formato correto
      const newBook = await createBook(bookData);
      reply.code(201).send(newBook);
    } catch (error) {
      reply.code(400).send({ message: 'Erro ao criar livro', error });
    }
  });
}

export default bookRoutes;
